import sys
from googletrans import Translator, LANGUAGES
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QVBoxLayout, QHBoxLayout, QWidget, QTextEdit

class TranslationApp(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Translate Everything into Mandarin!")
        self.setGeometry(100, 100, 800, 600)  # Size of the application window
        
        # Main layout
        main_layout = QHBoxLayout()
        
        # Button for starting translation
        self.translate_button = QPushButton("Start Translation", self)
        self.translate_button.clicked.connect(self.translate_text)
        
        # Split panel for input and output
        panel_layout = QVBoxLayout()
        
        # Text edit for input
        self.input_text = QTextEdit(self)
        self.input_text.setPlaceholderText("Enter text to translate...")
        
        # Text edit for output
        self.output_text = QTextEdit(self)
        self.output_text.setPlaceholderText("Translation will appear here...")
        self.output_text.setReadOnly(True)  # Make the output text edit read-only
        
        # Add widgets to the panel layout
        panel_layout.addWidget(self.input_text)
        panel_layout.addWidget(self.output_text)
        
        # Add the button and panel to the main layout
        main_layout.addWidget(self.translate_button)
        main_layout.addLayout(panel_layout)
        
        # Set main layout to central widget
        widget = QWidget()
        widget.setLayout(main_layout)
        self.setCentralWidget(widget)
        
        # Initialize the translator
        self.translator = Translator()

    def translate_text(self):
        source_text = self.input_text.toPlainText()
        # Translate the text, here we assume translating to German for example
        result = self.translator.translate(source_text, dest='zh-TW')
        self.output_text.setText(result.text)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    translator = TranslationApp()
    translator.show()
    sys.exit(app.exec_())
