import sys
import cv2
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QMainWindow, QLabel, QPushButton, QVBoxLayout, QHBoxLayout, QWidget, QSpacerItem, QSizePolicy
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QApplication

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("AI Demo App")
        self.setGeometry(100, 100, 600, 300)  # Adjusted size and position of App

        # Create a widget for window content
        central_widget = QWidget(self)
        self.setCentralWidget(central_widget)
        layout = QHBoxLayout(central_widget)  # Horizontal layout to place buttons on the left and video on the right

        # Vertical layout for buttons
        button_layout = QVBoxLayout()
        
        # Spacers to center buttons vertically
        top_spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        bottom_spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        
        # Buttons for eye tracking
        self.start_eye_button = QPushButton("Start Eye Tracking", self)
        self.start_eye_button.clicked.connect(self.start_eye_tracking)
        self.stop_eye_button = QPushButton("Stop Eye Tracking", self)
        self.stop_eye_button.clicked.connect(self.stop_eye_tracking)
        
        # Buttons for face tracking
        self.start_face_button = QPushButton("Start Face Tracking", self)
        self.start_face_button.clicked.connect(self.start_face_tracking)
        self.stop_face_button = QPushButton("Stop Face Tracking", self)
        self.stop_face_button.clicked.connect(self.stop_face_tracking)
        
        button_layout.addItem(top_spacer)  # Add top spacer
        button_layout.addWidget(self.start_eye_button)
        button_layout.addWidget(self.stop_eye_button)
        button_layout.addWidget(self.start_face_button)
        button_layout.addWidget(self.stop_face_button)
        button_layout.addItem(bottom_spacer)  # Add bottom spacer

        # Set up the label for displaying the camera feed
        self.image_label = QLabel(self)
        layout.addLayout(button_layout)
        layout.addWidget(self.image_label)

        # Set up the video capture and timer
        self.cap = cv2.VideoCapture(0)
        self.timer = QTimer()
        self.timer.timeout.connect(self.display_video_stream)
        self.timer.start(20)

        # Load cascades for eye and face detection
        self.eye_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_eye.xml')
        self.face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
        self.track_eyes = False
        self.track_faces = False

    def start_eye_tracking(self):
        self.track_eyes = True

    def stop_eye_tracking(self):
        self.track_eyes = False

    def start_face_tracking(self):
        self.track_faces = True

    def stop_face_tracking(self):
        self.track_faces = False

    def display_video_stream(self):
        _, frame = self.cap.read()
        frame = cv2.flip(frame, 1)
        frame = cv2.resize(frame, (400, 200))
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        if self.track_faces:
            # Detect faces
            faces = self.face_cascade.detectMultiScale(gray, 1.1, 4)
            for (x, y, w, h) in faces:
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

        if self.track_eyes:
            # Detect eyes
            eyes = self.eye_cascade.detectMultiScale(gray, 1.1, 4)
            for (x, y, w, h) in eyes:
                cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)

        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        image = QImage(frame, frame.shape[1], frame.shape[0], frame.strides[0], QImage.Format_RGB888)
        self.image_label.setPixmap(QPixmap.fromImage(image))

    def closeEvent(self, event):
        super().closeEvent(event)
        self.cap.release()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())
