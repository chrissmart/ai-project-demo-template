import sys
import numpy as np
import pyaudio
import speech_recognition as sr
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QVBoxLayout, QHBoxLayout, QWidget, QLabel

class MplCanvas(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        self.axes.set_facecolor('black')  # Set the background color of the plot to black
        fig.tight_layout()
        super(MplCanvas, self).__init__(fig)

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Type What You Hear")
        self.setGeometry(100, 100, 800, 600)  # Size of the application window

        # Main layout is horizontal to separate the button and canvas
        main_layout = QHBoxLayout()

        # Vertical layout for buttons and text display
        button_layout = QVBoxLayout()
        
        # Button to start/stop audio streaming
        self.audio_button = QPushButton("Start Audio Stream", self)
        self.audio_button.clicked.connect(self.toggle_audio_stream)
        button_layout.addWidget(self.audio_button)

        # Button to start speech to text
        self.speech_button = QPushButton("Start Speech to Text", self)
        self.speech_button.clicked.connect(self.start_speech_to_text)
        self.speech_button.setEnabled(False)  # Disable until recording starts
        button_layout.addWidget(self.speech_button)
        
        # Label to display speech to text results
        self.text_label = QLabel("Speech to text results appear here.", self)
        self.text_label.setWordWrap(True)  # Enable word wrap to handle long texts
        button_layout.addWidget(self.text_label)
        
        button_layout.addStretch()  # Add stretch to push everything to the top

        # Add button layout to the main layout
        main_layout.addLayout(button_layout)

        # Initialize audio stream
        self.audio_streaming = False
        self.audio = pyaudio.PyAudio()
        self.frames = []  # A buffer to store raw audio data

        # Matplotlib canvas setup using MplCanvas
        self.canvas = MplCanvas(self, width=5, height=4, dpi=100)
        self.line, = self.canvas.axes.plot(np.zeros(1024), color='green')  # Initialize line with green color
        main_layout.addWidget(self.canvas)

        # Set main layout to central widget
        widget = QWidget()
        widget.setLayout(main_layout)
        self.setCentralWidget(widget)

        # Initialize speech recognizer
        self.recognizer = sr.Recognizer()
        self.mic = sr.Microphone()

    def toggle_audio_stream(self):
        if not self.audio_streaming:
            self.audio_button.setText("Stop Audio Stream")
            self.audio_streaming = True
            self.speech_button.setEnabled(False)  # Disable speech recognition while recording
            self.stream = self.audio.open(format=pyaudio.paInt16,
                                          channels=1,
                                          rate=44100,
                                          input=True,
                                          frames_per_buffer=1024,
                                          stream_callback=self.audio_callback)
            self.frames = []  # Clear previous recordings
        else:
            self.audio_button.setText("Start Audio Stream")
            self.audio_streaming = False
            self.stream.stop_stream()
            self.stream.close()
            self.speech_button.setEnabled(True)  # Enable speech recognition after recording

    def audio_callback(self, in_data, frame_count, time_info, status):
        self.frames.append(in_data)  # Append raw audio data to the frame buffer
        audio_data = np.frombuffer(in_data, dtype=np.int16)
        self.line.set_ydata(audio_data)
        self.canvas.draw_idle()  # Efficiently update the canvas
        return (in_data, pyaudio.paContinue)

    def start_speech_to_text(self):
        audio_data = b''.join(self.frames)
        audio_stream = sr.AudioData(audio_data, 44100, 2)
        try:
            text = self.recognizer.recognize_google(audio_stream)
            self.text_label.setText(text)
        except sr.RequestError as e:
            self.text_label.setText("API unavailable.")
        except sr.UnknownValueError:
            self.text_label.setText("Unable to recognize speech.")
    
    def closeEvent(self, event):
        super().closeEvent(event)
        if self.audio_streaming:
            self.stream.stop_stream()
            self.stream.close()
        self.audio.terminate()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    sys.exit(app.exec_())
