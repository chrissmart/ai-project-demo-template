# AI Project Demo App Template

This repo includes three types of AI demo apps templates:
* Vision AI
* Audio AI
* Language AI

**Spec**
* Testing Platform: Mac M1
* Mac OS 14.4.1

## Setup
Build the virutal environment and install the required packages.
```
CONDA_SUBDIR=osx-arm64 conda create -n audio-ai python python=3.11.8
pip install --global-option='build_ext' --global-option="-I$(brew --prefix)/include" --global-option="-L$(brew --prefix)/lib" pyaudio
pip install -r requirements.txt
```

## Run the App
```
python vision_ai_app.py
python audio_ai_app.py
python language_ai_app.py
```

## Package the App
```
pyinstaller -w -F --clean audio_ai_app.py
pyinstaller -w -F --clean language_ai_app.py
```
