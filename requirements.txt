pyqt5==5.15.10
numpy
matplotlib
opencv-python==4.9.0.80
SpeechRecognition
googletrans==4.0.0-rc1
pyinstaller
